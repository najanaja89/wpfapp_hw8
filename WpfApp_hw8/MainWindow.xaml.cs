﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp_hw8;

using System.Globalization;
using System.IO;

namespace WpfApp_hw8
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var methods = new Methods();
            nameRoster.ItemsSource = null;
            nameRoster.ItemsSource = methods.UsersList();

        }

        private IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);

                    if (child != null && child is T)
                        yield return (T)child;

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                        yield return childOfChild;
                }
            }
        }

        private void ThemeButtonClick(object sender, RoutedEventArgs e)
        {
       
           
            if (themeButton.Content.ToString() == "Dark Theme")
            {
                themeButton.Content = "Light Theme";
                search.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#808080"));
                this.Resources = new ResourceDictionary() { Source = new Uri("StandartTheme.xaml", UriKind.RelativeOrAbsolute) };
               
                foreach (var item in FindVisualChildren<TextBlock>(nameRoster))
                {
                    if (item.Name == "subTextRoster")
                    {
                        item.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#808080")); ;
                    }
                }

                foreach (var item in FindVisualChildren<TextBlock>(nameRoster))
                {
                    if (item.Name == "firstName")
                    {
                        item.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#000000")); ;
                    }
                }
            }
            else
            {
                themeButton.Content = "Dark Theme";
                search.Foreground= new SolidColorBrush((Color)ColorConverter.ConvertFromString("#000000"));
                this.Resources = new ResourceDictionary() { Source = new Uri("DarkTheme.xaml", UriKind.RelativeOrAbsolute) };
                foreach (var item in FindVisualChildren<TextBlock>(nameRoster))
                {
                    if (item.Name == "subTextRoster")
                    {
                        item.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF")); ;
                    }
                }

                foreach (var item in FindVisualChildren<TextBlock>(nameRoster))
                {
                    if (item.Name == "firstName")
                    {
                        item.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFF"));
                    }
                }
            }

        }
    }
}
