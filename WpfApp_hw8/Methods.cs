﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_hw8
{
    public class Methods
    {
        public List<User> UsersList()
        {
            var users = new List<User>
            {
                new User { Name = "John", Surname="Wick"},
                new User { Name = "Bruce", Surname="Wayne"},
                new User { Name = "Harry", Surname="Potter"},
                new User { Name = "Peter", Surname="Parker"},
                new User { Name = "Clark", Surname="Kent"},
                new User { Name = "Jon", Surname="Snow"},
            };

            return users;
        }
    }
}
